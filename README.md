# Daddy-box
_A simplified, button-based interface for telegram voice chat for kids._

See [this article](https://chordata.cc/blog/open-source-intercom-for-kids/) for details and building instructions.

![daddy_box_assembled](https://chordata.cc/wp-content/uploads/2021/01/daddy_box_final-1024x576.jpg)

## Installation

This program was developed and tested in a Raspberry Pi model 3B+, in order to install it:

```bash
sudo apt install portaudio19-dev libffi-dev libssl-dev
python3 setup.py install
```
## Setup 

Before running this program you have to create a telegram bot key. Follow [these steps](https://core.telegram.org/bots#3-how-do-i-create-a-bot) to create your instance of a Bot, and then run this program with the `--setup-bot` flag and input the information the BotFather gave you.

You can now run the program with a _TUI_ to complete the setup.

```bash
python3 -m daddy_box --tui

```

You should now be able to search for the bot's username in telegram and exchange some messages with it. You will first find that you get "not allowed” responses. The idea of this bot is to exchange messages privately with just one user, so you have to tell the bot which is the allowed user id to interact with.

Take a look at the terminal, you will see some printed messages like this one:

![daddy box user id](https://chordata.cc/wp-content/uploads/2021/01/daddy_box_user_id.png)

Copy your user-id from there and give it to the bot using the `--setup-user` flag

You should now be able to send and receive voice messages, so you are ready to start with the physical part of the project, take a look at [the article](https://chordata.cc/blog/open-source-intercom-for-kids/) for the details.

## Hardware

See the [Daddy Box | RPi HAT repository](https://gitlab.com/daylanKifky/daddy-box-raspberry-pi-hat)

## Auto-start

Add the following line to /etc/rc.local

```bash
python3 -m daddy_box &
``` 

Restart your raspberry, you should see the leds doing a greeting sequence after boot.

If you get only a red button blinking at increasing intervals it mean there's a network problem and the program can't connect with the telegram API
