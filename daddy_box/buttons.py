from pynput.keyboard import Key
from enum import Enum

class btn_state(Enum):
	IDLE =0
	FOCUS = 1
	PRESSED = 3

class Buttons:
	def __init__(self, width, notify):
		self.log_win_width = width
		self.record = btn_state.FOCUS
		self.audios = [btn_state.IDLE]*4
		self.pos_v = 0
		self.pos_h = 0
		self.notify = notify #only for debugging

	def _move_audios(self):
		self.record = btn_state.IDLE
		self.audios = [btn_state.IDLE]*4
		self.audios[self.pos_h] = btn_state.FOCUS

	def end_press(self):
		if self.pos_v == 0:
			self.record = btn_state.FOCUS
			self.audios = [btn_state.IDLE]*4
		else:
			self._move_audios()

	@property
	def audio_pressed(self):
		for a in self.audios:
			if a == btn_state.PRESSED:
				return True

		return False

	@property
	def audio_position(self):
		return self.pos_h

	def handle_key(self, key):
		#change position
		if key == Key.up:
			if self.pos_v == 1: self.pos_v = 0  
			self.record = btn_state.FOCUS
			self.audios = [btn_state.IDLE]*4

		elif key == Key.down:
			if self.pos_v == 0: self.pos_v = 1
			self._move_audios()

		elif key == Key.left:
			self.pos_h -= 1
			self.pos_h = max(self.pos_h, 0)
			self._move_audios()

		elif key == Key.right:
			self.pos_h += 1
			self.pos_h = min(self.pos_h, 3)
			self._move_audios()

		#handle button press
		if key == Key.space:
			switch = lambda state: btn_state(state.value ^ 2)
			if self.record.value != 0:
				self.record = switch(self.record)

			for i in range(len(self.audios)):
				btn = self.audios[i]
				if btn.value != 0:
					self.audios[i] = switch(btn)


	def _btn_repr(self, btn):
		if btn == btn_state.IDLE:
			return "[   ]"
		if btn == btn_state.FOCUS:
			return "[ x ]"
		if btn == btn_state.PRESSED:
			return "[###]"


	def _get_btns(self):
		btns = [self._btn_repr(self.record)] 
		btns += [self._btn_repr(b) for b in self.audios]
		return btns

	def _get_btns_format(self):
		return """
{{0:_<{0:.0f}}}

{{1:^{0:.0f}}}
{{6:^{0:.0f}}}

{{7:^{1:.0f}}}{{8:^{1:.0f}}}{{9:^{1:.0f}}}{{10:^{1:.0f}}}
{{2:^{1:.0f}}}{{3:^{1:.0f}}}{{4:^{1:.0f}}}{{5:^{1:.0f}}} 
""".format(self.log_win_width, self.log_win_width/4)


	def __str__(self):
		return self._get_btns_format().format(
			"__| Controls |", "Record",
			"A1  ", "A2  ", "A3  ", "A4  ",
			*(self._get_btns()) ) + "_"*self.log_win_width