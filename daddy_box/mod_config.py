from .utils import abs_path

filename = abs_path('config.ini')

import configparser
config = configparser.ConfigParser()
config.read(filename)

def set_bot_token(new_token):
	config['CREDENTIALS']['bot_token'] = new_token 	
	with open(filename, 'w') as configfile:
		config.write(configfile)


def set_user_id(new_id):
	config['CREDENTIALS']['user_id'] = new_id 	
	with open(filename, 'w') as configfile:
		config.write(configfile)

def clear_config():
	config['CREDENTIALS']['bot_token'] = '0'
	config['CREDENTIALS']['user_id'] = 'unset' 	
	with open(filename, 'w') as configfile:
		config.write(configfile)


def get_bot_token():
	return config['CREDENTIALS']['bot_token']

def get_user_id():
	return config['CREDENTIALS']['user_id']

def print_config():
	print("BOT TOKEN:",config['CREDENTIALS']['bot_token'])
	print("USER ID:",config['CREDENTIALS']['user_id'])

