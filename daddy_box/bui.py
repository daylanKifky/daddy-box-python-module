from gpiozero import Button, LED
from signal import pause
import functools
from time import sleep, time
# import threading
# from collections import deque 
# from os import system
# from .buttons import Buttons, btn_state
from .leds import Leds
from .audio import Audio_Manager
from .utils import abs_path



bounce = 0.2

# buttons = [ Button(27, bounce_time=bounce), Button(9, bounce_time=bounce),
# 			Button(6, bounce_time=bounce),	Button(19, bounce_time=bounce),
# 			Button(24, bounce_time=bounce), Button(20, bounce_time=bounce) ]

buttons = [ Button(27), Button(9), Button(6),
		Button(19), Button(24), Button(20) ]


import logging
logging.basicConfig(filename=abs_path("session.log"),
					format='%(asctime)s (%(name)s|%(levelname)s) %(message)s',
					level=logging.DEBUG)

_key = 0
_msg = "<no message>"
term_width = 80

def notify(msg):
	logging.info("MSG: {}".format(msg))
	print("MSG: {}".format(msg))

def log(msg):
	logging.info("LOG: {}".format(msg))
	print("LOG: {}".format(msg))

def on_record_stop():
	#this function is meant to be overrided
	log("Warning: No stop handler!")


rec_filename = "audios/last_record.ogg"
sample_filename = "audios/test.wav"

rec_led = LED(23)
def end_press():
	rec_led.on()

def blink_rec_led():
	rec_led.on()
	sleep(0.5)
	rec_led.off()


audio = Audio_Manager(abs_path(rec_filename), log, end_press)
leds = Leds(term_width, audio)
pressed_btns = [False]*4
last_pressed = [time()]*4

def play_btn_pressed(num):
	global pressed_btns, last_pressed
	now = time()
	if now - last_pressed[num] < 0.5:
		return

	last_pressed[num] = now

	pressed_btns[num]= True
	log("Button {} pressed".format(num))
	found_double_press = False
	
	if not leds.stand_by:
		for i,btn in enumerate(pressed_btns):
			if btn and i != num:
				found_double_press = True
				break

	if found_double_press:
		audio.stop()
		leds.stand_by = True
		rec_led.off()
		return

	if leds.stand_by:
		rec_led.on()
		leds.stand_by = False
		return

	leds.listen_audio(num)
	audio.play(num)

def play_btn_released(num):
	global pressed_btns
	pressed_btns[num] = False

for i in range(4):
	buttons[i].when_pressed = functools.partial(play_btn_pressed, i)
	buttons[i].when_released = functools.partial(play_btn_released, i)

def press_rec():
	if leds.stand_by:
		return
	rec_led.off()
	audio.record()

def release_rec():
	if leds.stand_by:
		return
	rec_led.on()
	audio.stop()
	on_record_stop()

buttons[4].when_pressed = press_rec 
buttons[4].when_released = release_rec
end_press()

def print_header():
	print("Raspberry telegram communicator")
	print("===============================")
	print("Started with Button interface")
	s = "/ Bot log \\"
	sl = len(s)-2
	print(("{:>"+str(term_width-2)+"}").format("_"*sl))
	print(("{:>"+str(term_width-1)+"}").format(s))
	print("-"*term_width)


def start():
	print_header()
	leds.greeting()
	leds.start()
	audio.add_received(sample_filename)
	leds.receive_audio()
	audio.play(0)
	leds.listen_audio(0)

def run():
	leds.wait_no_more()
	pause()
	leds.stop()

# -----------  STANDBY STATE  -----------



# _keep_waiting = True

# def wait():
# 	global _keep_waiting
# 	while _keep_waiting:

# 		leds.wait()