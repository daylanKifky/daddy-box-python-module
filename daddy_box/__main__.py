import datetime
from threading import Condition
from sys import exit

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from . import config
from .utils import abs_path
from .audio import print_devices


import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--tui", help="Run with terminal user interface", action="store_true")
parser.add_argument("--devices", help="Print available audio devices and exit", action="store_true")
parser.add_argument("--bot", help="Get Bot information and exit", action="store_true")
parser.add_argument("--setup-bot", help="Setup the bot token")
parser.add_argument("--setup-user", help="Setup the user id")
parser.add_argument("--clear-config", help="Clear bot token and user-id", action="store_true")


args = parser.parse_args()

if args.devices:
	print_devices()
	exit(0)

if args.clear_config:
	from . import mod_config
	mod_config.clear_config()
	exit(0)

if args.setup_bot:
	from . import mod_config
	mod_config.set_bot_token(args.setup_bot)
	exit(0)

if args.setup_user:
	from . import mod_config
	mod_config.set_user_id(args.setup_user)
	exit(0)


if args.tui:
	from . import tui as ui
else:
	from . import bui as ui


try:
	updater = Updater(token=config.bot_token )
except Exception as e:
	print("The bot token [{}] is invalid".format(config.bot_token))
	print("run the module with the --setup-bot flag")
	exit(1)

dispatcher = updater.dispatcher

received_voice_filename = "audios/{}-received_voice.oga"
not_allowed_msg = "This bot is not for you"
CHAT_ID = config.user_id
START_DATETIME = datetime.datetime.now().isoformat()

_bot_started = Condition()

def start(update, context):
	global CHAT_ID
	greeting = "Raspberry communicator started"
	user_id = update.effective_user.id
	user_name = update.effective_user.username
	chat_id = update.effective_chat.id
	if user_id != config.user_id:
		context.bot.send_message(chat_id=chat_id, text=not_allowed_msg)
		error = "Received unauthorized /start from [{}] id:{}".format(user_name, user_id)
		ui.log(error)
		context.bot.send_message(chat_id=CHAT_ID, text=error)
		return

	CHAT_ID = chat_id
	ui.log("======= Bot start =======")
	ui.log("Chat ID: {} | User ID: {}, NAME: {}".format(chat_id, user_id, user_name))

	with _bot_started:
		_bot_started.notify()

	context.bot.send_message(chat_id=chat_id, text=greeting)

start_handler = CommandHandler('start', start)
dispatcher.add_handler(start_handler)


def status(update, context):
	global CHAT_ID
	user_id = update.effective_user.id
	user_name = update.effective_user.username
	chat_id = update.effective_chat.id
	if user_id != config.user_id:
		context.bot.send_message(chat_id=chat_id, text=not_allowed_msg)
		error = "Received unauthorized /status from [{}] id:{}".format(user_name, user_id)
		ui.log(error)
		context.bot.send_message(chat_id=CHAT_ID, text=error)
		return

	ui.log("======= Status request =======")

	response = "Up from {}\n== Last received==\n".format(START_DATETIME)
	for received in ui.audio.last_received:
		response += received + '\n'

	response += '== Last sent==\n'

	for sent in ui.audio.last_recordings:
		response += sent + '\n'

	context.bot.send_message(chat_id=chat_id, text=response)
	ui.log(response)

status_handler = CommandHandler('status', status)
dispatcher.add_handler(status_handler)


def echo(update, context):
	response = "This bot is made to exchange voice messages, but thanks for writting anyway"
	chat_id = update.effective_chat.id
	user_id = update.effective_user.id
	user_name = update.effective_user.username
	text = update.message.text
	if user_id != config.user_id:
		context.bot.send_message(chat_id=chat_id, text=not_allowed_msg)
		error = "Received unauthorized text from [{}] id:{}".format(user_name, user_id)
		ui.log(error)
		context.bot.send_message(chat_id=CHAT_ID, text=error)
		return

	ui.notify("Receive text from [{}]: {}".format(user_name, text))
	context.bot.send_message(chat_id=chat_id, text=response)

echo_handler = MessageHandler(Filters.text & (~Filters.command), echo)
dispatcher.add_handler(echo_handler)
	

def send_voice():
	if not CHAT_ID:
		ui.log("The bot should be /start(ed) first to send a voice message")
		return		

	ui.log("Sending voice message")
	try:
		if ui.audio.valid_recording:
			updater.bot.send_voice(chat_id=CHAT_ID, voice=open(abs_path(ui.rec_filename), 'rb'))
	except Exception as e:
		ui.log("Error sending voice: {}".format(e))
		updater.bot.send_message(chat_id=CHAT_ID, text="problem sending voice")

ui.on_record_stop = send_voice

def receive_voice(update, context):
	chat_id = update.effective_chat.id
	user_id = update.effective_user.id
	user_name = update.effective_user.username
	voice = update.effective_message.voice
	voice_id = voice.file_id

	if user_id != config.user_id:
		context.bot.send_message(chat_id=chat_id, text=not_allowed_msg)
		error = "Received unauthorized voice from [{}] id:{}".format(user_name, user_id)
		ui.log(error)
		context.bot.send_message(chat_id=CHAT_ID, text=error)
		return

	ui.notify("Receive voice from [{}], duration: {:.2f}s".format(user_name, voice.duration))

	file = context.bot.get_file(voice_id)
	d = file.download_as_bytearray()

	filename = received_voice_filename.format(datetime.datetime.now().isoformat())

	with open(abs_path(filename), 'wb') as received:
		received.write(d)

	ui.log("Written voice file {}, size: {}".format(filename, len(d)))

	ui.audio.add_received(filename)
	ui.leds.receive_audio()


voice_handler = MessageHandler(Filters.voice, receive_voice)
dispatcher.add_handler(voice_handler)

if __name__ == '__main__':
	bot_info = None
	retry_conn = 1
	while not bot_info:
		try:
			bot_info = updater.bot.get_me()
			print(bot_info)
		except Exception as e:
			from time import sleep
			print("Could not get Bot info. Check the internet connection. Retrying in {}secs".format(retry_conn))	
			if "blink_rec_led" in ui.__dict__:
				ui.blink_rec_led()
			sleep(retry_conn)
			retry_conn = min(retry_conn*2, 30)

	if not args.bot:
		updater.start_polling()
		if "start" in ui.__dict__:
			ui.start()

		while not CHAT_ID:
			ui.log("Waiting for user to /start bot")
			with _bot_started:
					_bot_started.wait()

		print("Bot /started by user ")			

		# from signal import pause
		# pause()
		ui.run()

		print("Cleaning and stopping telegram updater. Press Ctrl+C to force close")
		updater.stop()


