from os import path

abs_path = lambda filename: path.join(path.dirname(path.realpath(__file__)), filename)