from pynput.keyboard import Key, KeyCode, Listener
import threading
from time import sleep
from collections import deque 
from os import system
from .buttons import Buttons, btn_state
from .leds import Leds
from .audio import Audio_Manager
from .utils import abs_path

import logging
logging.basicConfig(filename=abs_path("session.log"),
					format='%(asctime)s (%(name)s|%(levelname)s) %(message)s',
					level=logging.DEBUG)

_cv = threading.Condition()
_key = 0
_msg = "<no message>"
_log = deque(maxlen=5)

for i in range(_log.maxlen):
	_log.append('~')

term_width = 80
Q_KEY = KeyCode.from_char('q')

def notify(msg):
	logging.info("MSG: {}".format(msg))
	global _msg
	with _cv:
		_msg = msg
		_log.appendleft(msg)
		_cv.notify()

def log(msg):
	logging.info("LOG: {}".format(msg))
	global _log
	with _cv:
		_log.appendleft(msg)
		_cv.notify()


rec_filename = "audios/last_record.ogg"
sample_filename = "audios/test.wav"

buttons = Buttons(term_width, notify)
audio = Audio_Manager(abs_path(rec_filename), log, buttons.end_press)
audio.add_received(sample_filename)
leds = Leds(term_width, audio)

def print_header():
	system('clear')
	print("Raspberry telegram communicator TUI")
	print("===================================")
	print("Move with arrows, press with SPACE (press Q to exit)")
	s = "/ Bot log \\"
	sl = len(s)-2
	print(("{:>"+str(term_width-2)+"}").format("_"*sl))
	print(("{:>"+str(term_width-1)+"}").format(s))
	print("-"*term_width)
	for i in range(_log.maxlen):
		print(("| {:"+str(term_width-4)+"} |").format(_log[ _log.maxlen - i - 1]))
	print("-"*term_width)
	print("--> message:", _msg)
	print(buttons)
	print(leds)

def on_record_stop():
	#this function is meant to be overrided
	log("Warning: No stop handler!")

def on_press(key):
	global _key, _log

	if _key == Q_KEY:
		return False

	buttons.handle_key(key)

	if not audio.is_recording and buttons.record == btn_state.PRESSED:
		audio.record()
		leds.start_rec()

	if audio.is_recording and buttons.record != btn_state.PRESSED:
		audio.stop()
		leds.end_rec()
		on_record_stop()

	if not audio.is_playing and buttons.audio_pressed:
		audio.play(buttons.audio_position)
		# import pdb; pdb.set_trace()
		leds.listen_audio(buttons.audio_position)

	if audio.is_playing and not buttons.audio_pressed:
		audio.stop()

	with _cv:
		_key = key
		_cv.notify()


def run():
	print_header()

	# Collect events until released
	with Listener(on_press=on_press) as listener:
		while _key != Q_KEY:
			with _cv:
				_cv.wait()
				
			print_header()

			print('{0} pressed'.format(
				_key))

		print("Stopping key listener")
		listener.stop()

	listener.join()
