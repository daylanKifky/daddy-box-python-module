import pyaudio
import wave
import threading
import datetime
from collections import deque
from os import system, path, remove, listdir

from .utils import abs_path

#from https://realpython.com/playing-and-recording-sound-python/#recording-audio

class Audio_Manager():
	def __init__(self, _rec_file, _log, _clear_btn_press):
		self.chunk = 256  # Record in chunks of 1024 samples
		self.sample_format = pyaudio.paInt16  # 16 bits per sample
		self.channels = 1
		self.fs = 48000  # Record at 48000 samples per second
		self.seconds = 120
		self.rec_file = _rec_file
		self.is_recording = False
		self.valid_recording = False

		self.received = deque(maxlen=4)
		self.is_playing = False

		self.log = _log
		self.clear_btn_press = _clear_btn_press

		self.last_recordings = deque(maxlen=10)
		self.last_received = deque(maxlen=4)


	def add_received(self, filename):
		try:
			abs_filename = abs_path(filename)
			if filename.endswith(".oga"):
				#OPUS encoded OGG is not easy to decode with python tools
				#use external opus-tools instead
				in_file = abs_path(filename)
				abs_filename =  abs_filename.replace(".oga", ".wav")
				system("opusdec --gain 16 {} {}".format(in_file, abs_filename))

			self.received.appendleft(abs_filename)
			self.last_received.appendleft(filename)
			self.log("Append received to Audio_Manager: [{}]".format(filename))

			# Remove unused files
			removed = 0
			for f in listdir(path.join(path.dirname(__file__), "audios")):
				the_file = abs_path(path.join("audios", f))
				if path.isfile(the_file):
					if the_file not in self.received and the_file != self.rec_file:
						remove(the_file)
						removed += 1

			if removed:
				self.log("Removed {} unused audio files".format(removed))

		except Exception as e:
			self.log("Error converting received OGG to WAV: {}".format(e))



	def _play(self, position):
		try:
			self.log("Playback at position {} started".format(position))
			self.is_playing = True
			filename = self.received[position]
			wf = wave.open(filename, 'rb')
			p = pyaudio.PyAudio()
			stream = p.open(format = p.get_format_from_width(wf.getsampwidth()),
							channels = wf.getnchannels(),
							rate = wf.getframerate(),
							output = True,
							output_device_index = 0)
			data = wf.readframes(self.chunk)
			self.log("About to play audio")
			while self.is_playing and data != b'':
				stream.write(data)
				data = wf.readframes(self.chunk)
				# self.log("playing {} | {}".format(self.is_playing, data != ''))


		except IndexError as e:
			self.log("No audio at position {}".format(position))

		except Exception as e:
			self.log("Error while playing audio: {}".format(e))



		self.is_playing = False
		if 'stream' in locals(): 
			stream.close()

		if 'p' in locals(): 
			p.terminate()

		self.clear_btn_press()
		self.log("Playback ended")

	def play(self, position):
		if self.is_playing or self.is_recording:
			return
		self.t_play = threading.Thread(target=self._play, args=(position,))
		self.t_play.start()

	def _record(self):
		self.is_recording = True
		p = pyaudio.PyAudio()  # Create an interface to PortAudio
		stream = p.open(format=self.sample_format,
						channels=self.channels,
						rate=self.fs,
						frames_per_buffer=self.chunk,
						input=True)

		frames = []  # Initialize array to store frames

		self.log("Record started")

		# Store data in chunks
		for i in range(0, int(self.fs / self.chunk * self.seconds)):
			if not self.is_recording:
				break
			data = stream.read(self.chunk)
			frames.append(data)

		if self.is_recording:
			self.log("Record ended (max time = {}secs)".format(self.seconds))
		else:
			self.log("Recorder stopped")
			self.is_recording = False

		self.clear_btn_press()

		# Stop and close the stream 
		stream.stop_stream()
		stream.close()

		p.terminate()
		data_length = len(frames)*self.chunk
		if data_length/self.fs < 1.5:
			self.valid_recording = False
			self.log("Unvalid recording, duration: {:.2f}secs".format(data_length/self.fs))
			return

		# Save the recorded data as a WAV file
		wav_file =  self.rec_file.replace(".ogg", ".wav")
		wf = wave.open(wav_file, 'wb')
		wf.setnchannels(self.channels)
		wf.setsampwidth(p.get_sample_size(self.sample_format))
		wf.setframerate(self.fs)
		frame_string = b''.join(frames)
		wf.writeframes(frame_string)
		wf.close()

		# Convert the recorded data to an OGG file
		system("opusenc {} {}".format(wav_file, self.rec_file))
		remove(wav_file)


		self.valid_recording = True
		self.log("Saved record file, duration: {:.2f}secs".format(data_length/self.fs))
		self.last_recordings.appendleft("{}, duration: {:.2f}secs".format(datetime.datetime.now().isoformat(), data_length/self.fs))

	def record(self):
		if self.is_playing or self.is_recording:
			return
		self.t_rec = threading.Thread(target=self._record)
		self.t_rec.start()


	def stop(self):
		if hasattr(self, 't_rec'):
			self.is_recording = False
			self.t_rec.join()

		if hasattr(self, 't_play'):
			self.is_playing = False
			self.t_play.join()

		self.log("all audio threads ended")


def print_devices():
	p = pyaudio.PyAudio()
	for ii in range(p.get_device_count()):
		print("AUDIO DEVICE:", ii, p.get_device_info_by_index(ii).get('name'))
		devrate = int(p.get_device_info_by_index(ii).get('defaultSampleRate'))
		print("DEVICE RATE:", devrate)