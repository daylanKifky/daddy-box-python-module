from enum import Enum, auto
from collections import deque
from time import sleep
import threading

try:
	from gpiozero import Button, LED

	real_leds = [ LED(17), LED(10), LED(5), LED(13)]
		# , , LED(16) ]
except Exception as e:
	print("Warning! No gpiozero library found")

class led_state(Enum):
	EMPTY = auto()
	NEW = auto()
	FULL = auto()


class Leds:
	def __init__(self, width, audio):
		self.log_win_width = width
		self.audio = audio
		self.record = led_state.EMPTY
		self.received = deque([led_state.EMPTY]*4, maxlen=4)
		self.blink = True
		self.still_waiting = True
		self.wait_state = 0
		for i, recv in enumerate(audio.received):
			self.received[i] = led_state.NEW

		self.keep_updating = True
		self.t = threading.Thread(target=self.update_display)

		self.stand_by = False

	def start(self):
		self.t.start()

	def stop(self):
		self.keep_updating = False
		self.t.join()

	def wait_no_more(self):
		self.still_waiting = False
		print("LEDS wait no more")

	def update_display(self):
		while self.keep_updating:
			if self.stand_by:
				for led in real_leds:
					led.off()
				sleep(1)
				continue

			if not self.still_waiting:
				sleep(0.5)
				self.display_leds()
				self.blink = not self.blink
			else:
				sleep(0.6)
				self.wait_state = (self.wait_state + 1) % 4
				for i,led in enumerate(real_leds):
					if i == self.wait_state:
						led.on()
					else:
						led.off()

	def display_leds(self):
		for i,led in enumerate(real_leds):
			if self.received[i] == led_state.EMPTY:
				led.off()
			elif self.received[i] == led_state.FULL:
				led.on()
			else:
				if self.blink:
					led.on()
				else:
					led.off()

	def greeting(self):
		for l in real_leds:
			l.on()
			sleep(0.2)

		for l in real_leds:
			sleep(0.4)
			l.off()

		sleep(1)
		for l in real_leds:
			l.on()

		sleep(1)
		for l in real_leds:
			l.off()

	def start_rec(self):
		self.record = led_state.FULL

	def end_rec(self):
		self.record = led_state.EMPTY

	def receive_audio(self):
		self.received.appendleft(led_state.NEW)
		self.display_leds()

	def listen_audio(self, position):
		if len(self.audio.received) > position:
			self.received[position]=led_state.FULL
		self.display_leds()

	def _led_repr(self, led):
		if led == led_state.EMPTY:
			return "(   )"
		if led == led_state.NEW:
			return "(~%~)"
		if led == led_state.FULL:
			return "( % )"

	def _get_leds(self):
		leds = [self._led_repr(self.record)] 
		leds += [self._led_repr(b) for b in self.received]
		return leds

	def _get_leds_format(self):
			return """
{{0:_<{0:.0f}}}

{{1:^{0:.0f}}}
{{6:^{0:.0f}}}

{{7:^{1:.0f}}}{{8:^{1:.0f}}}{{9:^{1:.0f}}}{{10:^{1:.0f}}}
{{2:^{1:.0f}}}{{3:^{1:.0f}}}{{4:^{1:.0f}}}{{5:^{1:.0f}}} 
""".format(self.log_win_width, self.log_win_width/4)


	def __str__(self):
		return self._get_leds_format().format(
			"__| Leds |", "Record",
			"A1  ", "A2  ", "A3  ", "A4  ",
			*(self._get_leds()) ) + "_"*self.log_win_width