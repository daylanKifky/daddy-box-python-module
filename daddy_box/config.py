from .mod_config import get_bot_token, get_user_id

bot_token = get_bot_token()
user_id = int(get_user_id())
